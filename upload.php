<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021-3-21
 * Time: 12:10
 */
$file        = $_FILES['file'];//得到传输的数据
$type        = $_GET['type'];
$upload_path = "file" . '/' . $type . '/' . date('Ym');

is_dir($upload_path) || mkdir($upload_path, 0777, true);
chmod($upload_path, 0777);
$name = $file['name'];

$rs = move_uploaded_file($file['tmp_name'], $upload_path . '/' . $file['name']);
if ($rs) {
    echo json_encode(['code' => 10001, 'msg' => 'success:上传成功']);
} else {
    echo json_encode(['code' => 10000, 'msg' => 'error:上传失败']);
}
?>